<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>ログイン</title>
</head>
<body>
<header>
<h1>ログイン</h1>
ID パスワードを入力してください。
</header>

<form method="post"  action="login" >
<label>ID<input type="text" name="id" maxlength="1000"/></label>
<label>パスワード<input type="password" name="pass" maxlength="72"/></label>
<input type="submit" />
</form>
</body>
</html>
